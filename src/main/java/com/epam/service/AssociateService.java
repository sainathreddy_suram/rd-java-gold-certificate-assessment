package com.epam.service;

import java.util.List;

import com.epam.dto.AssociateDto;

public interface AssociateService {

	public AssociateDto save(AssociateDto associateDto);

	public AssociateDto update(AssociateDto associateDto);

	public void delete(Integer id);

	public AssociateDto view(Integer id);

	public List<AssociateDto> viewByGender(String gender);
}
