package com.epam.service;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.epam.converter.AssociateConvertor;
import com.epam.dto.AssociateDto;
import com.epam.entity.Associate;
import com.epam.exceptions.AssociateException;
import com.epam.exceptions.AssociateNotFoundException;
import com.epam.exceptions.BatchNotFoundException;
import com.epam.repository.AssociateDao;
import com.epam.repository.BatchDao;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class AsscociateServiceImpl implements AssociateService {

	private final BatchDao batchDao;
	private final AssociateConvertor associateConvertor;
	private final AssociateDao associateDao;

	public AsscociateServiceImpl(BatchDao batchDao, AssociateConvertor associateConvertor, AssociateDao associateDao) {
		super();
		this.batchDao = batchDao;
		this.associateConvertor = associateConvertor;
		this.associateDao = associateDao;
	}

	public AssociateDto save(AssociateDto associateDto) {
		log.info("Entered save method in " + this.getClass());
		try {
			genderCheck(associateDto);
			final Associate associate = associateConvertor.associateDtoToAssociateConverter(associateDto);
			addBatch(associateDto, associate);
			log.info("Leaving save method in " + this.getClass());
			return associateConvertor.associateToAssociateDtoConverter(associateDao.save(associate));
		} catch (DataIntegrityViolationException e) {
			throw new AssociateException("Associate with given name " + associateDto.getName()
					+ " is already present, Please give correct Input values");
		} catch (BatchNotFoundException e) {
			throw new BatchNotFoundException("No Batch with given id is present");

		} catch (Exception e) {
			throw new AssociateException(e.getMessage() + " Please give Correct Input Values");

		}

	}

	private void addBatch(AssociateDto associateDto, final Associate associate) {
		associate.setBatch(batchDao.findById(associateDto.getBatchId())
				.orElseThrow(() -> new BatchNotFoundException("No Batch with given Id is present")));
	}

	public AssociateDto update(AssociateDto associateDto) {

		log.info("Entered update method in " + this.getClass());
		if (associateDto != null && associateDao.existsById(associateDto.getId())) {
			genderCheck(associateDto);
			final Associate associate = associateConvertor.associateDtoToAssociateConverter(associateDto);

			addBatch(associateDto, associate);
			log.info("Leaving update method in " + this.getClass());
			return associateConvertor.associateToAssociateDtoConverter(associateDao.save(associate));
		} else {
			throw new AssociateException("Associate with given Id is not present, Please give correct Input values");
		}

	}

	private void genderCheck(AssociateDto associateDto) {
		if (associateDto.getGender()==null ||!(associateDto.getGender().equalsIgnoreCase("M") || associateDto.getGender().equalsIgnoreCase("F"))) {
			associateDto.setGender("Other");
		}
	}

	public void delete(Integer id) {
		log.info("Entered delete method in " + this.getClass());
		associateDao.deleteById(id);
		log.info("Leaving delete method in " + this.getClass());

	}

	public AssociateDto view(Integer id) {
		log.info("Entered view method in " + this.getClass());
		log.info("Leaving view method in " + this.getClass());
		return associateConvertor.associateToAssociateDtoConverter(associateDao.findById(id)
				.orElseThrow(() -> new AssociateNotFoundException("No Associate with given Id is present ")));
	}

	public List<AssociateDto> viewByGender(String gender) {
		log.info("Entered viewByGender method in " + this.getClass());
		if (gender != null
				&& (gender.equalsIgnoreCase("M") || gender.equalsIgnoreCase("F") || gender.equalsIgnoreCase("Other"))) {
			log.info("Leaving viewByGender method in " + this.getClass());

			return associateDao.findByGender(gender).stream().map(associateConvertor::associateToAssociateDtoConverter)
					.toList();
		} else {
			throw new AssociateException("Enter correct  input values");
		}
	}

}
