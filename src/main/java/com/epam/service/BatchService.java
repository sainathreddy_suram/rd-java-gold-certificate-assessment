package com.epam.service;

import com.epam.dto.BatchDtoForRead;
import com.epam.dto.BatchDtoForWrite;

public interface BatchService {
	public BatchDtoForRead save(BatchDtoForWrite batchDto);

	public BatchDtoForRead update(BatchDtoForWrite batchDto);

	public void delete(Integer id);

	public BatchDtoForRead view(Integer id);
}
