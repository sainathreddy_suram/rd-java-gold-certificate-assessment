package com.epam.service;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.epam.converter.BatchConvertor;
import com.epam.dto.BatchDtoForRead;
import com.epam.dto.BatchDtoForWrite;
import com.epam.entity.Batch;
import com.epam.exceptions.BatchException;
import com.epam.exceptions.BatchNotFoundException;
import com.epam.repository.BatchDao;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class BatchServiceImpl implements BatchService {

	private final BatchDao batchDao;
	private final BatchConvertor batchConvertor;

	public BatchServiceImpl(BatchDao batchDao, BatchConvertor batchConvertor) {
		super();
		this.batchDao = batchDao;
		this.batchConvertor = batchConvertor;
	}

	public BatchDtoForRead save(BatchDtoForWrite batchDto) {
		log.info("Entered save method in " + this.getClass());
		try {
			final Batch batch = batchConvertor.batchDtoToBatchConverter(batchDto);
			log.info("Leaving save method in " + this.getClass());
			return batchConvertor.batchToBatchDtoConverter(batchDao.save(batch));
		} catch (DataIntegrityViolationException e) {
			throw new BatchException(
					"Batch with given Name " + batchDto.getName() + " is already present, Please give Correct Inputs");
		} catch (Exception e) {
			throw new BatchException(e.getMessage()+" Please give correct Input values");
		}

	}

	public BatchDtoForRead update(BatchDtoForWrite batchDto) {
		log.info("Entered update method in " + this.getClass());
		if (batchDto != null && batchDao.existsById(batchDto.getId())) {
			final Batch batch = batchConvertor.batchDtoToBatchConverter(batchDto);
			log.info("Leaving update method in " + this.getClass());
			return batchConvertor.batchToBatchDtoConverter(batchDao.save(batch));
		} else {
			throw new BatchException("Batch with given name is not present, Please give correct name");
		}

	}

	public void delete(Integer id) {
		log.info("Entered delete method in " + this.getClass());
		batchDao.deleteById(id);
		log.info("Leaving delete method in " + this.getClass());
	}

	public BatchDtoForRead view(Integer id) {
		log.info("Entered view method in " + this.getClass());
		log.info("Leaving view method in " + this.getClass());
		return batchConvertor.batchToBatchDtoConverter(batchDao.findById(id)
				.orElseThrow(() -> new BatchNotFoundException("No Batch with given ID  is present")));
	}

}
