package com.epam.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BatchDtoForWrite {

	@NotNull(message = "id cannot be Null")
	private Integer id;
	@NotNull(message = "name cannot be Null")
	private String name;
	@NotNull(message = "practice cannot be Null")
	private String practice;
	@NotNull (message = "startDate cannot be Null")
	private String startDate;
	@NotNull(message = "endDate cannot be null")
	private String endDate;

}
