package com.epam.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AssociateDto {

	@NotNull(message = "id cannot be Null")
	private int id;
	@NotNull(message = "name cannot be Null")
	private String name;
	@Email(message = "Give The email in Correct Format")
	private String email;
	@NotNull(message = "gender cannot be Null")
	private String gender;
	@NotNull(message = "college cannot be Null")
	private String college;
	@NotNull(message = "status cannot be Null")
	private String status;
	
	private Integer batchId;

}
