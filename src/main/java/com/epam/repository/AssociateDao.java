package com.epam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.entity.Associate;

public interface AssociateDao extends JpaRepository<Associate, Integer> {

	List<Associate> findByGender(String gender);
}
