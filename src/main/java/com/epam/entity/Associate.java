package com.epam.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
public class Associate {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotNull(message = "id cannot be Null")
	@Column(unique = true)
	@NotNull(message = "name cannot be Null")
	private String name;
	@Email(message = "Give The email in Correct Format")
	private String email;
	@NotNull(message = "gender cannot be Null")
	private String gender;
	@NotNull(message = "college cannot be Null")
	private String college;
	@NotNull(message = "status cannot be Null")
	private String status;

	@JsonIgnore
	@ManyToOne
	private Batch batch;

}
