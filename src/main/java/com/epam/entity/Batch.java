package com.epam.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Batch  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotNull(message = "id cannot be Null")
	@Column(unique = true)
	@NotNull(message = "name cannot be Null")
	private String name;
	@NotNull(message = "practice cannot be Null")
	private String practice;
	@NotNull(message = "startDate cannot be Null")
	private LocalDate startDate;
	@NotNull(message = "endDate cannot be null")
	private LocalDate endDate;
	@OneToMany(mappedBy = "batch", cascade = { CascadeType.MERGE, CascadeType.DETACH })
	private List<Associate> associates = new ArrayList<>();

}
