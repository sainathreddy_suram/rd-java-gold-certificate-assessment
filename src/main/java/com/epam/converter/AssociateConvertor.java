package com.epam.converter;

import org.springframework.stereotype.Component;

import com.epam.dto.AssociateDto;
import com.epam.entity.Associate;

@Component
public class AssociateConvertor {

	public Associate associateDtoToAssociateConverter(AssociateDto associateDto) {
		Associate associate = new Associate();
		associate.setId(associateDto.getId());
		associate.setName(associateDto.getName());
		associate.setEmail(associateDto.getEmail());
		associate.setGender(associateDto.getGender());
		associate.setStatus(associateDto.getStatus());
		associate.setCollege(associateDto.getCollege());

		return associate;
	}

	public AssociateDto associateToAssociateDtoConverter(Associate associate) {
		AssociateDto associateDto = new AssociateDto();
		associateDto.setId(associate.getId());
		associateDto.setName(associate.getName());
		associateDto.setEmail(associate.getEmail());
		associateDto.setGender(associate.getGender());
		associateDto.setStatus(associate.getStatus());
		associateDto.setCollege(associate.getCollege());
		if(associate.getBatch()!= null) {
		associateDto.setBatchId(associate.getBatch().getId());
		}
	

		return associateDto;
	}

}
