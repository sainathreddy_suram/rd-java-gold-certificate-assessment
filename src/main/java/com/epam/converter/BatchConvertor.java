package com.epam.converter;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.epam.dto.BatchDtoForRead;
import com.epam.dto.BatchDtoForWrite;
import com.epam.entity.Batch;

@Component
public class BatchConvertor {

	public Batch batchDtoToBatchConverter(BatchDtoForWrite batchDto) {
		Batch batch = new Batch();
		batch.setId(batchDto.getId());
		batch.setName(batchDto.getName());
		batch.setPractice(batchDto.getPractice());
		LocalDate date = LocalDate.parse(batchDto.getStartDate());
		batch.setStartDate(date);
		date = LocalDate.parse(batchDto.getEndDate());
		batch.setEndDate(date);
		

		return batch;
	}

	public BatchDtoForRead batchToBatchDtoConverter(Batch batch) {
		BatchDtoForRead batchDto = new BatchDtoForRead();
		batchDto.setId(batch.getId());
		batchDto.setName(batch.getName());
		batchDto.setPractice(batch.getPractice());
		batchDto.setStartDate(batch.getStartDate().toString());
		batchDto.setEndDate(batch.getEndDate().toString());
		batchDto.setAssociates(batch.getAssociates());
		return batchDto;
	}

}
