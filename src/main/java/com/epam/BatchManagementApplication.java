package com.epam;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "BatchManagement System", contact = @Contact(email = "sainathreddy_suram@epam.com", name = "Suram Sri sainath Reddy")))
public class BatchManagementApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(BatchManagementApplication.class, args);
		context.getBeanDefinitionCount();
	}

	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
	}

}
