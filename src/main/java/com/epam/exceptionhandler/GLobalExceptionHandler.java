package com.epam.exceptionhandler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.exceptions.AssociateException;
import com.epam.exceptions.AssociateNotFoundException;
import com.epam.exceptions.BatchException;
import com.epam.exceptions.BatchNotFoundException;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class GLobalExceptionHandler {

	@ExceptionHandler(BatchException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleBatchException(BatchException e, WebRequest request, HttpServletRequest serrequest) {
		log.error(e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), e.getMessage(),
				serrequest.getLocalAddr());
	}

	@ExceptionHandler(BatchNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ExceptionResponse handleBatchNotFoundException(BatchNotFoundException e, WebRequest request,
			HttpServletRequest serrequest) {
		log.error(e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.NOT_FOUND.toString(), e.getMessage(),
				serrequest.getLocalAddr());
	}

	@ExceptionHandler(AssociateException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleAssociateException(AssociateException e, WebRequest request,
			HttpServletRequest serrequest) {
		log.error(e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), e.getMessage(),
				serrequest.getLocalAddr());
	}

	@ExceptionHandler(AssociateNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ExceptionResponse handleAssociateNotFoundException(AssociateNotFoundException e, WebRequest request,
			HttpServletRequest serrequest) {
		log.error(e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.NOT_FOUND.toString(), e.getMessage(),
				serrequest.getLocalAddr());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e,
			WebRequest request) {
		log.error(e.getMessage());
		StringBuilder inputerror = new StringBuilder();
		e.getAllErrors().forEach(err -> inputerror.append(err.getDefaultMessage() + "\n"));
		log.error(e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), inputerror.toString(),
				request.getDescription(false));
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionResponse handleRuntimeException(RuntimeException e, WebRequest request) {
		log.error(e.getMessage());
		return new ExceptionResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.toString(), e.getMessage(),
				request.getDescription(false));

	}
}
