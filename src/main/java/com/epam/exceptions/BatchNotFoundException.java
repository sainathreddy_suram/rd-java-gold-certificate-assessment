package com.epam.exceptions;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class BatchNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public BatchNotFoundException(String message) {
		super(message);
		log.info(message);
	}
}
