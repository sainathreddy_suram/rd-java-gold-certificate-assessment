package com.epam.exceptions;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class AssociateNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public AssociateNotFoundException(String message) {
		super(message);
		log.info(message);
	}
}
