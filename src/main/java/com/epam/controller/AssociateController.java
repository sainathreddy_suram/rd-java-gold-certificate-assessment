package com.epam.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociateDto;
import com.epam.service.AssociateService;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/rd/associates")
public class AssociateController {

	private final AssociateService associateService;

	public AssociateController(AssociateService associateService) {

		this.associateService = associateService;
	}

	@PostMapping
	@Operation(description = "Method to add an associate, Insert Gender as Either M/F,Enter Email in correct Format,Insert Status as Either ACTIVE/INACTIVE ")
	public ResponseEntity<AssociateDto> save(@RequestBody @Valid AssociateDto associateDto) {
		log.info("Entered save method in " + this.getClass());
		return new ResponseEntity<>(associateService.save(associateDto), HttpStatus.CREATED);
	}

	@PutMapping
	@Operation(description = "Method to update an associate, Insert gender as Either M/F,Enter Email in correct Format,Insert Status as Either ACTIVE/INACTIVE  ")
	public ResponseEntity<AssociateDto> update(@RequestBody @Valid AssociateDto associateDto) {
		log.info("Entered update method in " + this.getClass());
		return new ResponseEntity<>(associateService.update(associateDto), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	@Operation(description = "Method to delete an associate")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		log.info("Entered delete method in " + this.getClass());
		associateService.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping
	@Operation(description = "Method to view an associate's data")
	public ResponseEntity<AssociateDto> view(@RequestParam Integer id) {
		log.info("Entered view method in " + this.getClass());
		return new ResponseEntity<>(associateService.view(id), HttpStatus.OK);
	}

	@GetMapping("/{gender}")
	@Operation(description = "Method to view associate's data based on Gender, Insert Gender as Either M/F/Other")
	public ResponseEntity<List<AssociateDto>> view(@PathVariable String gender) {
		log.info("Entered view method in " + this.getClass());
		return new ResponseEntity<>(associateService.viewByGender(gender), HttpStatus.OK);
	}

}
