package com.epam.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.BatchDtoForRead;
import com.epam.dto.BatchDtoForWrite;
import com.epam.service.BatchService;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;



@RestController
@Slf4j
@RequestMapping("/batchs")
public class BatchController {

	private final BatchService batchService;

	public BatchController(BatchService batchService) {

		this.batchService = batchService;
	}

	@PostMapping
	@Operation(description = "Method to save a Batch,add Dates in the format of yyyy-mm-dd")
	public ResponseEntity<BatchDtoForRead> save(@RequestBody @Valid BatchDtoForWrite batchDto) {
		log.info("Entered save method in " + this.getClass());
		return new ResponseEntity<>(batchService.save(batchDto), HttpStatus.CREATED);
	}

	@PutMapping
	@Operation(description = "Method to update a Batch's details,add Dates in the format of yyyy-mm-dd")
	public ResponseEntity<BatchDtoForRead> update(@RequestBody @Valid BatchDtoForWrite batchDto) {
		log.info("Entered update method in " + this.getClass());
		return new ResponseEntity<>(batchService.update(batchDto), HttpStatus.OK);
	}

	@DeleteMapping
	public ResponseEntity<Void> delete(@RequestParam Integer id) {
		log.info("Entered delete method in " + this.getClass());
		batchService.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping
	public ResponseEntity<BatchDtoForRead> view(@RequestParam Integer id) {
		log.info("Entered view method in " + this.getClass());
		return new ResponseEntity<>(batchService.view(id), HttpStatus.OK);
	}

}
