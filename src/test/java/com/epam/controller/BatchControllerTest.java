package com.epam.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.epam.dto.BatchDtoForRead;
import com.epam.dto.BatchDtoForWrite;
import com.epam.exceptions.BatchException;
import com.epam.exceptions.BatchNotFoundException;
import com.epam.service.BatchService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = BatchController.class)
class BatchControllerTest {

	@MockBean
	private BatchService batchService;

	@Autowired
	private MockMvc mockMvc;
	private BatchDtoForRead batchDtoForRead;
	private BatchDtoForWrite batchDtoForWrite;
	private ObjectMapper objectMapper = new ObjectMapper();

	@BeforeEach
	void setup() {
	
		batchDtoForRead = new BatchDtoForRead();
		batchDtoForRead.setId(1);
		batchDtoForRead.setName("hii");
		batchDtoForRead.setPractice("java");
		batchDtoForRead.setStartDate("12-09-2001");
		batchDtoForRead.setEndDate("12-09-2001");
		
		batchDtoForWrite = new BatchDtoForWrite();
		batchDtoForWrite.setId(1);
		batchDtoForWrite.setName("hii");
		batchDtoForWrite.setPractice("java");
		batchDtoForWrite.setStartDate("12-09-2001");
		batchDtoForWrite.setEndDate("12-09-2001");
	}

	@Test
	void saveTestForCorrectValues() throws Exception {
		Mockito.when(batchService.save(any())).thenReturn(batchDtoForRead);

		MvcResult mvcResult = mockMvc
				.perform(post("/batchs").content(objectMapper.writeValueAsString(batchDtoForWrite))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("hii")))

				.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();
		String batchDtoJson = objectMapper.writeValueAsString(batchDtoForRead);
		assertEquals(mvcResult.getResponse().getContentAsString(), batchDtoJson);
		assertNotNull(mvcResult);
		Mockito.verify(batchService).save(any());

	}

	@Test
	void saveTestForIncorrectValues() throws Exception {
		Mockito.when(batchService.save(any())).thenThrow(BatchException.class);

		MvcResult mvcResult = mockMvc
				.perform(post("/batchs").content(objectMapper.writeValueAsString(batchDtoForWrite))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andReturn();
		assertNotNull(mvcResult);

		Mockito.verify(batchService).save(any());

	}

	@Test
	void updateTestForCorrectValues() throws Exception {
		Mockito.when(batchService.update(any())).thenReturn(batchDtoForRead);

		MvcResult mvcResult = mockMvc
				.perform(put("/batchs").content(objectMapper.writeValueAsString(batchDtoForWrite))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("hii")))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		String batchDtoJson = objectMapper.writeValueAsString(batchDtoForRead);
		assertEquals(mvcResult.getResponse().getContentAsString(), batchDtoJson);
		assertNotNull(mvcResult);
		Mockito.verify(batchService).update(any());

	}

	@Test
	void updateTestForIncorrectValues() throws Exception {
		Mockito.when(batchService.update(any())).thenThrow(BatchException.class);

		MvcResult mvcResult = mockMvc
				.perform(put("/batchs").content(objectMapper.writeValueAsString(batchDtoForWrite))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andReturn();
		assertNotNull(mvcResult);

		Mockito.verify(batchService).update(any());

	}

	@Test

	void testdeleteBatch() throws Exception {

		MvcResult mvcResult = mockMvc.perform(delete("/batchs").param("id", "1"))
				.andExpect(status().isNoContent()).andReturn();
		assertNotNull(mvcResult);
		Mockito.verify(batchService).delete(any());
	}

	@Test
	void testdisplayBatch() throws Exception {
		Mockito.when(batchService.view(any())).thenReturn(batchDtoForRead);

		MvcResult mvcResult = mockMvc.perform(get("/batchs").param("id", "1"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("hii")))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		System.out.println(mvcResult.getResponse().getContentAsString());
		assertNotNull(mvcResult);
		Mockito.verify(batchService).view(any());
	}

	@Test
	void testdisplayForIncorrect() throws Exception {
		Mockito.when(batchService.view(any())).thenThrow(BatchNotFoundException.class);

		MvcResult mvcResult = mockMvc.perform(get("/batchs").param("id", "1"))
				.andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
		assertNotNull(mvcResult);
		Mockito.verify(batchService).view(any());
	}

	@Test
	void testdisplayForIncorrectValues() throws Exception {
		Mockito.when(batchService.view(any())).thenThrow(RuntimeException.class);

		MvcResult mvcResult = mockMvc.perform(get("/batchs").param("id", "1"))
				.andExpect(MockMvcResultMatchers.status().isInternalServerError()).andReturn();
		assertNotNull(mvcResult);
		Mockito.verify(batchService).view(any());
	}
	
	

}
