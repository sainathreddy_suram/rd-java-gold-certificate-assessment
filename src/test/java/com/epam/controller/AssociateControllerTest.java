package com.epam.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.epam.dto.AssociateDto;
import com.epam.entity.Associate;
import com.epam.exceptions.AssociateException;
import com.epam.exceptions.AssociateNotFoundException;
import com.epam.service.AssociateService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = AssociateController.class)
class AssociateControllerTest {

	@MockBean
	private AssociateService associateService;

	@Autowired
	private MockMvc mockMvc;
	private Associate associate;
	private AssociateDto associateDto;
	private ObjectMapper objectMapper = new ObjectMapper();

	@BeforeEach
	void setup() {

		associate = new Associate();
		associate.setId(1);
		associate.setName("hii");
		associate.setEmail("Java@gmail.com");
		associate.setGender("M");
		associate.setStatus("Active");
		associate.setCollege("hello");
		associateDto = new AssociateDto();
		associateDto.setId(1);
		associateDto.setName("hii");
		associateDto.setEmail("Java@gmail.com");
		associateDto.setGender("m");
		associateDto.setStatus("Active");
		associateDto.setCollege("hello");

	}

	@Test
	void saveTestForCorrectValues() throws Exception {
		Mockito.when(associateService.save(any())).thenReturn(associateDto);

		MvcResult mvcResult = mockMvc
				.perform(post("/rd/associates").content(objectMapper.writeValueAsString(associateDto))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("hii")))

				.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();
		String associateDtoJson = objectMapper.writeValueAsString(associateDto);
		assertEquals(mvcResult.getResponse().getContentAsString(), associateDtoJson);
		assertNotNull(mvcResult);
		Mockito.verify(associateService).save(any());

	}

	@Test
	void saveTestForIncorrectValues() throws Exception {
		Mockito.when(associateService.save(any())).thenThrow(AssociateException.class);

		MvcResult mvcResult = mockMvc.perform(post("/rd/associates")
				.content(objectMapper.writeValueAsString(associateDto)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andReturn();
		assertNotNull(mvcResult);

		Mockito.verify(associateService).save(any());

	}

	@Test
	void updateTestForCorrectValues() throws Exception {
		Mockito.when(associateService.update(any())).thenReturn(associateDto);

		MvcResult mvcResult = mockMvc
				.perform(put("/rd/associates").content(objectMapper.writeValueAsString(associateDto))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("hii")))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		String associateDtoJson = objectMapper.writeValueAsString(associateDto);
		assertEquals(mvcResult.getResponse().getContentAsString(), associateDtoJson);
		assertNotNull(mvcResult);
		Mockito.verify(associateService).update(any());

	}

	@Test
	void updateTestForInCorrectFormatValues() throws Exception {
		Mockito.when(associateService.update(any())).thenReturn(associateDto);
		associateDto.setEmail("hii");
		MvcResult mvcResult = mockMvc
				.perform(put("/rd/associates").content(objectMapper.writeValueAsString(associateDto))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();

		assertNotNull(mvcResult);

	}

	@Test
	void updateTestForIncorrectValues() throws Exception {
		Mockito.when(associateService.update(any())).thenThrow(AssociateException.class);

		MvcResult mvcResult = mockMvc.perform(put("/rd/associates")
				.content(objectMapper.writeValueAsString(associateDto)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andReturn();
		assertNotNull(mvcResult);

		Mockito.verify(associateService).update(any());

	}

	@Test
	void testdeleteAssociate() throws Exception {

		MvcResult mvcResult = mockMvc.perform(delete("/rd/associates/1")).andExpect(status().isNoContent()).andReturn();
		assertNotNull(mvcResult);
		Mockito.verify(associateService).delete(any());
	}

	@Test
	void testdisplayAssociate() throws Exception {
		Mockito.when(associateService.view(any())).thenReturn(associateDto);

		MvcResult mvcResult = mockMvc.perform(get("/rd/associates").param("id", "1"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("hii")))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		System.out.println(mvcResult.getResponse().getContentAsString());
		assertNotNull(mvcResult);
		Mockito.verify(associateService).view(any());
	}

	@Test
	void testdisplayForIncorrect() throws Exception {
		Mockito.when(associateService.view(any())).thenThrow(AssociateNotFoundException.class);

		MvcResult mvcResult = mockMvc.perform(get("/rd/associates").param("id", "1"))
				.andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
		assertNotNull(mvcResult);
		Mockito.verify(associateService).view(any());
	}

	@Test
	void testdisplayByGenderAssociate() throws Exception {
		List<AssociateDto> list = List.of(associateDto);
		Mockito.when(associateService.viewByGender(any())).thenReturn(list);

		MvcResult mvcResult = mockMvc.perform(get("/rd/associates/M")).andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();
		System.out.println(mvcResult.getResponse().getContentAsString());
		assertNotNull(mvcResult);
		assertEquals(objectMapper.writeValueAsString(list), mvcResult.getResponse().getContentAsString());
		Mockito.verify(associateService).viewByGender(any());
	}

	@Test
	void testdisplayByGenderForIncorrect() throws Exception {
		Mockito.when(associateService.viewByGender(any())).thenThrow(AssociateNotFoundException.class);

		MvcResult mvcResult = mockMvc.perform(get("/rd/associates/l"))
				.andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
		assertNotNull(mvcResult);
		Mockito.verify(associateService).viewByGender(any());
	}
	
	@Test
	void testdisplayByGenderForNullValues() throws Exception {
		Mockito.when(associateService.viewByGender(any())).thenThrow(AssociateNotFoundException.class);

		MvcResult mvcResult = mockMvc.perform(get("/rd/associates/null"))
				.andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
		assertNotNull(mvcResult);
		Mockito.verify(associateService).viewByGender(any());
	}

}
