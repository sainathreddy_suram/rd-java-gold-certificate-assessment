package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;

import com.epam.converter.AssociateConvertor;
import com.epam.dto.AssociateDto;
import com.epam.entity.Associate;
import com.epam.entity.Batch;
import com.epam.exceptions.AssociateException;
import com.epam.exceptions.AssociateNotFoundException;
import com.epam.exceptions.BatchNotFoundException;
import com.epam.repository.AssociateDao;
import com.epam.repository.BatchDao;

@ExtendWith(MockitoExtension.class)
class AssociateServiceTest {
	@Mock
	private AssociateDao associateDao;
	@Mock
	private AssociateConvertor associateConvertor;
	@Mock
	private BatchDao batchDao;
	@InjectMocks
	private AsscociateServiceImpl asscociateService;

	private Associate associate;
	private AssociateDto associateDto;
	private Batch batch;
	private Optional<Batch> optionalBatch;

	@BeforeEach
	void setup() {
		batch = new Batch(1, "hello", "java", null, null, null);
		optionalBatch = Optional.of(batch);
		associate = new Associate();
		associate.setId(1);
		associate.setName("hello");
		associate.setStatus("Active");
		associate.setGender("F");

		associateDto = new AssociateDto();
		associateDto.setId(1);
		associateDto.setName("hello");
		associateDto.setStatus("Active");
		associateDto.setGender("M");

	}

	@Test
	void saveTestForCorrectValues() {
		associateDto.setGender(null);
		Mockito.when(associateConvertor.associateDtoToAssociateConverter(associateDto)).thenReturn(associate);
		Mockito.when(batchDao.findById(any())).thenReturn(optionalBatch);
		Mockito.when(associateDao.save(associate)).thenReturn(associate);
		Mockito.when(associateConvertor.associateToAssociateDtoConverter(associate)).thenReturn(associateDto);

		AssociateDto result = asscociateService.save(associateDto);
		assertEquals(associateDto, result);
		Mockito.verify(associateDao).save(associate);
	}

	@Test
	void saveTestForIncorrectBatchValues() {
		associateDto.setGender("L");
		Mockito.when(associateConvertor.associateDtoToAssociateConverter(associateDto)).thenReturn(associate);
		Mockito.when(batchDao.findById(any())).thenReturn(Optional.empty());
		assertThrows(BatchNotFoundException.class, () -> asscociateService.save(associateDto));

	}

	@Test
	void saveTestForNullValues() {
		//Mockito.when(associateConvertor.associateDtoToAssociateConverter(null)).thenThrow(NullPointerException.class);
		assertThrows(AssociateException.class, () -> asscociateService.save(null));

	}

	@Test
	void saveTestForSimilarAssociateNames() {
		Mockito.when(associateConvertor.associateDtoToAssociateConverter(associateDto)).thenReturn(associate);
		Mockito.when(batchDao.findById(any())).thenReturn(optionalBatch);
		Mockito.when(associateDao.save(associate)).thenThrow(DataIntegrityViolationException.class);
		assertThrows(AssociateException.class, () -> asscociateService.save(associateDto));
		Mockito.verify(associateDao).save(any());
	}

	@Test
	void updateTestForCorrectValues() {
		associateDto.setGender("F");
		Mockito.when(associateDao.existsById(associateDto.getId())).thenReturn(true);
		Mockito.when(associateConvertor.associateDtoToAssociateConverter(associateDto)).thenReturn(associate);
		Mockito.when(batchDao.findById(any())).thenReturn(optionalBatch);

		Mockito.when(associateDao.save(associate)).thenReturn(associate);
		Mockito.when(associateConvertor.associateToAssociateDtoConverter(associate)).thenReturn(associateDto);

		AssociateDto result = asscociateService.update(associateDto);
		assertEquals(associateDto, result);

	}

	@Test
	void updateTestForIncorrectBatchValues() {
		associateDto.setGender("M");
		Mockito.when(associateDao.existsById(associateDto.getId())).thenReturn(true);
		Mockito.when(associateConvertor.associateDtoToAssociateConverter(associateDto)).thenReturn(associate);
		Mockito.when(batchDao.findById(any())).thenReturn(Optional.empty());

		assertThrows(BatchNotFoundException.class, () -> asscociateService.update(associateDto));

	}

	@Test
	void updateTestForNotExistingValues() {

		Mockito.when(associateDao.existsById(associateDto.getId())).thenReturn(false);
		assertThrows(AssociateException.class, () -> asscociateService.update(associateDto));

	}

	@Test
	void updateTestForNullValues() {

		assertThrows(AssociateException.class, () -> asscociateService.update(null));

	}

	@Test
	void deleteTestForCorrectValues() {
		asscociateService.delete(1);
		Mockito.verify(associateDao, times(1)).deleteById(anyInt());
	}

	@Test
	void deleteTestForNullValues() {
		asscociateService.delete(null);
		Mockito.verify(associateDao, times(1)).deleteById(any());
	}

	@Test
	void viewTestForCorrectValues() {
		Optional<Associate> optionalAssociate = Optional.of(associate);
		Mockito.when(associateDao.findById(anyInt())).thenReturn(optionalAssociate);

		Mockito.when(associateConvertor.associateToAssociateDtoConverter(optionalAssociate.get()))
				.thenReturn(associateDto);

		AssociateDto result = asscociateService.view(associate.getId());
		assertEquals(associateDto, result);
		Mockito.verify(associateDao).findById(anyInt());
	}

	@Test
	void viewTestForNullValues() {

		Mockito.when(associateDao.findById(null)).thenReturn(Optional.empty());
		assertThrows(AssociateNotFoundException.class, () -> asscociateService.view(null));

		Mockito.verify(associateDao).findById(any());
	}

	@Test
	void viewByGenderTestForFemaleValues() {
		List<Associate> list = List.of(associate);
		Mockito.when(associateDao.findByGender(anyString())).thenReturn(list);

		Mockito.when(associateConvertor.associateToAssociateDtoConverter(any())).thenReturn(associateDto);

		List<AssociateDto> result = asscociateService.viewByGender("F");
		assertEquals(associateDto, result.get(0));
		Mockito.verify(associateDao).findByGender(anyString());
	}
	@Test
	void viewByGenderTestForMaleValues() {
		associate.setGender("M");
		List<Associate> list = List.of(associate);
		Mockito.when(associateDao.findByGender(anyString())).thenReturn(list);

		Mockito.when(associateConvertor.associateToAssociateDtoConverter(any())).thenReturn(associateDto);

		List<AssociateDto> result = asscociateService.viewByGender("M");
		assertEquals(associateDto, result.get(0));
		Mockito.verify(associateDao).findByGender(anyString());
	}
	
	@Test
	void viewByGenderTestForOtherValues() {
		associate.setGender("Other");
		List<Associate> list = List.of(associate);
		Mockito.when(associateDao.findByGender(anyString())).thenReturn(list);

		Mockito.when(associateConvertor.associateToAssociateDtoConverter(any())).thenReturn(associateDto);

		List<AssociateDto> result = asscociateService.viewByGender("Other");
		assertEquals(associateDto, result.get(0));
		Mockito.verify(associateDao).findByGender(anyString());
	}

	@Test
	void viewByGenderTestForNullValues() {

		assertThrows(AssociateException.class, () -> asscociateService.viewByGender(null));

	}
	
	@Test
	void viewByGenderTestForIncorrectValues() {

		assertThrows(AssociateException.class, () -> asscociateService.viewByGender("L"));

	}

}
