package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;

import com.epam.converter.BatchConvertor;
import com.epam.dto.BatchDtoForRead;
import com.epam.dto.BatchDtoForWrite;
import com.epam.entity.Batch;
import com.epam.exceptions.BatchException;
import com.epam.exceptions.BatchNotFoundException;
import com.epam.repository.BatchDao;

@ExtendWith(MockitoExtension.class)
class BatchServiceTest {
	@Mock
	private BatchDao batchDao;
	@Mock
	private BatchConvertor batchConvertor;
	
	@InjectMocks
	private BatchServiceImpl batchService;
	private Batch batch;
	private BatchDtoForRead batchDtoForRead;
	private BatchDtoForWrite batchDtoForWrite;

	@BeforeEach
	void setup() {
		batch = new Batch(1, "hello", null, null, null, null);
		batchDtoForRead = new BatchDtoForRead(null, null, null, null, null, null);
		batchDtoForRead.setName("hii");
		batchDtoForWrite = new BatchDtoForWrite(null, null, null, null, null);
		batchDtoForRead.setName("hii");
	}

	@Test
	void saveTestForCorrectValues() {
		
		Mockito.when(batchConvertor.batchDtoToBatchConverter(batchDtoForWrite)).thenReturn(batch);
		Mockito.when(batchDao.save(batch)).thenReturn(batch);
		Mockito.when(batchConvertor.batchToBatchDtoConverter(batch)).thenReturn(batchDtoForRead);
		BatchDtoForRead result = batchService.save(batchDtoForWrite);
		assertEquals(batchDtoForRead, result);
		Mockito.verify(batchDao).save(batch);
	}

	@Test
	void saveTestForNullValues() {
		Mockito.when(batchConvertor.batchDtoToBatchConverter(null)).thenThrow(NullPointerException.class);
		assertThrows(BatchException.class, () -> batchService.save(null));

	}

	@Test
	void saveTestForSimilarBatchIds() {
		Mockito.when(batchConvertor.batchDtoToBatchConverter(batchDtoForWrite)).thenReturn(batch);
		Mockito.when(batchDao.save(batch)).thenThrow(DataIntegrityViolationException.class);
		assertThrows(BatchException.class, () -> batchService.save(batchDtoForWrite));
		Mockito.verify(batchDao).save(any());
	}

	@Test
	void updateTestForCorrectValues() {

		Mockito.when(batchDao.existsById(batchDtoForWrite.getId())).thenReturn(true);
		Mockito.when(batchConvertor.batchDtoToBatchConverter(batchDtoForWrite)).thenReturn(batch);
		Mockito.when(batchDao.save(batch)).thenReturn(batch);

		Mockito.when(batchConvertor.batchToBatchDtoConverter(batch)).thenReturn(batchDtoForRead);

		BatchDtoForRead result = batchService.update(batchDtoForWrite);
		assertEquals(batchDtoForRead, result);

	}

	@Test
	void updateTestForNotExistingValues() {

		Mockito.when(batchDao.existsById(batchDtoForWrite.getId())).thenReturn(false);
		assertThrows(BatchException.class, () -> batchService.update(batchDtoForWrite));

	}

	@Test
	void updateTestForNullValues() {

		assertThrows(BatchException.class, () -> batchService.update(null));

	}

	@Test
	void deleteTestForCorrectValues() {
		batchService.delete(1);
		Mockito.verify(batchDao, times(1)).deleteById(anyInt());
	}

	@Test
	void deleteTestForNullValues() {
		batchService.delete(null);
		Mockito.verify(batchDao, times(1)).deleteById(null);
	}

	@Test
	void viewTestForCorrectValues() {
		Optional<Batch> optionalBatch = Optional.of(batch);
		Mockito.when(batchDao.findById(anyInt())).thenReturn(optionalBatch);
		Mockito.when(batchConvertor.batchToBatchDtoConverter(optionalBatch.get())).thenReturn(batchDtoForRead);

		BatchDtoForRead result = batchService.view(batch.getId());
		assertEquals(batchDtoForRead, result);
		Mockito.verify(batchDao).findById(anyInt());
	}

	@Test
	void viewTestForNullValues() {

		Mockito.when(batchDao.findById(null)).thenReturn(Optional.empty());
		assertThrows(BatchNotFoundException.class, () -> batchService.view(null));

		Mockito.verify(batchDao).findById(any());
	}

}
