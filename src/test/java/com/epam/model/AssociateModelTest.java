package com.epam.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.converter.AssociateConvertor;
import com.epam.dto.AssociateDto;
import com.epam.entity.Associate;
import com.epam.entity.Batch;

@ExtendWith(MockitoExtension.class)
class AssociateModelTest {

	Associate associate;
	AssociateDto associateDto;
	Batch batch;
	Optional<Batch> optionalBatch;

	@BeforeEach
	void setup() {
		batch = new Batch();
		batch.setId(1);
		associate = new Associate();
		associate.setId(1);
		associate.setName("hii");
		associate.setEmail("Java@gmail.com");
		associate.setGender("m");
		associate.setStatus("Active");
		associate.setBatch(null);
		associate.setCollege("hello");
		associate.getClass();
		AssociateConvertor associateConvertor = new AssociateConvertor();
		associateDto = associateConvertor.associateToAssociateDtoConverter(associate);
		associate = associateConvertor.associateDtoToAssociateConverter(associateDto);
		associate.setBatch(batch);
		associateConvertor.associateToAssociateDtoConverter(associate);
	}

	@Test
	void test() {
		assertEquals("hii", associate.getName());
		assertEquals("m", associate.getGender());
		assertEquals("Active", associate.getStatus());
		assertEquals("Java@gmail.com", associate.getEmail());
		assertEquals(batch, associate.getBatch());
		assertEquals(1, associate.getId());
		assertNotNull(associate.toString());
		assertEquals("hello", associate.getCollege());
	
		
	}

}
