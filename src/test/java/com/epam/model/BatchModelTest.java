package com.epam.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.converter.BatchConvertor;
import com.epam.dto.BatchDtoForRead;
import com.epam.dto.BatchDtoForWrite;
import com.epam.entity.Batch;

@ExtendWith(MockitoExtension.class)
class BatchModelTest {

	Batch batch;
	BatchDtoForRead batchDtoForRead;
	BatchDtoForWrite batchDtoForWrite;

	@BeforeEach
	void setup() {

		BatchConvertor batchConvertor = new BatchConvertor();
		batchDtoForWrite = new BatchDtoForWrite();
		batchDtoForWrite.setId(1);
		batchDtoForWrite.setName("hello");
		batchDtoForWrite.setEndDate("2007-12-03");
		batchDtoForWrite.setStartDate("2007-12-03");
		batchDtoForWrite.setPractice("java");

		batch = batchConvertor.batchDtoToBatchConverter(batchDtoForWrite);
		batch.setAssociates(null);
		batchDtoForRead = batchConvertor.batchToBatchDtoConverter(batch);

	}

	@Test
	void test() {
		assertEquals("hello", batch.getName());
		assertEquals(null, batch.getAssociates());
		assertEquals(1, batch.getId());
		assertNotNull(batch.toString());
	}

}
